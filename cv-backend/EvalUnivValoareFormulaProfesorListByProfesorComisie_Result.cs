//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CvBackend
{
    using System;
    
    public partial class EvalUnivValoareFormulaProfesorListByProfesorComisie_Result
    {
        public Nullable<decimal> Valoare { get; set; }
        public Nullable<int> Numar { get; set; }
        public Nullable<bool> ValIndeplinitConf { get; set; }
        public Nullable<bool> ValIndeplinitProf { get; set; }
        public Nullable<decimal> Suma { get; set; }
        public Nullable<bool> NrIndeplinitConf { get; set; }
        public Nullable<bool> NrIndeplinitProf { get; set; }
        public string Denumire { get; set; }
        public string FormulaCalcul { get; set; }
        public decimal ValMinConf { get; set; }
        public decimal ValMinProf { get; set; }
        public int NrMinConf { get; set; }
        public int NrMinProf { get; set; }
        public long ID_FormulaCentralizator { get; set; }
        public Nullable<decimal> SimulareValMinProf { get; set; }
        public Nullable<int> SimulareNrMinProf { get; set; }
        public Nullable<decimal> SimulareVal { get; set; }
        public Nullable<int> SimulareNumar { get; set; }
        public Nullable<bool> SimulareValIndeplinitProf { get; set; }
        public Nullable<bool> SimulareNrIndeplinitProf { get; set; }
    }
}
