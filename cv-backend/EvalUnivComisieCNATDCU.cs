//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CvBackend
{
    using System;
    using System.Collections.Generic;
    
    public partial class EvalUnivComisieCNATDCU
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EvalUnivComisieCNATDCU()
        {
            this.EvalUnivFormulaCentralizators = new HashSet<EvalUnivFormulaCentralizator>();
            this.EvalUnivProfesorComisieCNATDCUs = new HashSet<EvalUnivProfesorComisieCNATDCU>();
        }
    
        public long ID_EvalUnivComisieCNATDCU { get; set; }
        public string DenumireComisieCNATDCU { get; set; }
        public Nullable<long> ID_AnUniv { get; set; }
        public Nullable<int> AnCalendaristic { get; set; }
        public string CodComisie { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EvalUnivFormulaCentralizator> EvalUnivFormulaCentralizators { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EvalUnivProfesorComisieCNATDCU> EvalUnivProfesorComisieCNATDCUs { get; set; }
    }
}
