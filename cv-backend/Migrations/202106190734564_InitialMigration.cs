﻿namespace CvBackend.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cvs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IdProfesor = c.Long(nullable: false),
                        Json = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Cvs");
        }
    }
}
