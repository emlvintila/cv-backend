﻿using System.Globalization;

namespace CvBackend
{
    public static class StringExtensions
    {
        public static bool ContainsCaseInsensitive(this string source, string value, CultureInfo cultureInfo)
        {
            return cultureInfo.CompareInfo.IndexOf(source, value, CompareOptions.IgnoreCase) >= 0;
        }
    }
}
