//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CvBackend
{
    using System;
    using System.Collections.Generic;
    
    public partial class EvalUnivGrupProfesorPunctaj
    {
        public long ID_EvalUnivGrupProfesorPunctaj { get; set; }
        public long ID_EvalUnivGrupDetaliu { get; set; }
        public long ID_EvalUnivCriteriuProfesor { get; set; }
        public string FormulaCalculCompletataPunctaj { get; set; }
        public Nullable<decimal> PunctajGrupProfesor { get; set; }
    
        public virtual EvalUnivCriteriuProfesor EvalUnivCriteriuProfesor { get; set; }
        public virtual EvalUnivGrupDetaliu EvalUnivGrupDetaliu { get; set; }
    }
}
