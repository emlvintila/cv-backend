﻿using System;

namespace CvBackend.Models
{
    [Serializable]
    public class Journal
    {
        private string publicationTitle;
        private string journalAbbreviation;
        // ReSharper disable once InconsistentNaming
        private string ISSN;

        public string PublicationTitle
        {
            get => publicationTitle;
            set => publicationTitle = value;
        }

        public string JournalAbbreviation
        {
            get => journalAbbreviation;
            set => journalAbbreviation = value;
        }

        public string Issn
        {
            get => ISSN;
            set => ISSN = value;
        }

        public Journal(string publicationTitle, string journalAbbreviation, string issn)
        {
            PublicationTitle = publicationTitle;
            JournalAbbreviation = journalAbbreviation;
            Issn = issn;
        }
    }
}
