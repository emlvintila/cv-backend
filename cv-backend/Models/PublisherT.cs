﻿using System;

namespace CvBackend.Models
{
    [Serializable]
    public class PublisherT
    {
        private string publisher;

        public PublisherT(string publisher)
        {
            Publisher = publisher;
        }

        public string Publisher
        {
            get => publisher;
            set => publisher = value;
        }
    }
}
