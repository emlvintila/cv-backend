﻿using System.ComponentModel.DataAnnotations;

namespace CvBackend.Models
{
    public class Cv
    {
        [Key]
        public long Id { get; set; }    
        public long IdProfesor { get; set; }
        public string Json { get; set; }
    }
}
