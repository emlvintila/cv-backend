﻿using System.Data.Entity;
using CvBackend.Models;

namespace CvBackend
{
    public class CvContext : DbContext
    {
        public DbSet<Cv> Cvs { get; set; }
    }
}
