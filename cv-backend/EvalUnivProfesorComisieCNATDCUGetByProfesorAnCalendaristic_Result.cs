//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CvBackend
{
    using System;
    
    public partial class EvalUnivProfesorComisieCNATDCUGetByProfesorAnCalendaristic_Result
    {
        public long ID_ProfesorComisieCNATDCU { get; set; }
        public long ID_Profesor { get; set; }
        public long ID_EvalUnivComisieCNATDCU { get; set; }
        public long ID_AnUniv { get; set; }
        public int AnCalendaristic { get; set; }
    }
}
