//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CvBackend
{
    using System;
    
    public partial class EvalUnivPunctajeGradAnGet_Result
    {
        public long ID_EvalUnivPunctajeGradAn { get; set; }
        public Nullable<long> AnCalendaristic { get; set; }
        public Nullable<long> ID_TipGradDidactic { get; set; }
        public Nullable<decimal> PunctajDeIndeplinit { get; set; }
        public string TipCriteriu { get; set; }
    }
}
