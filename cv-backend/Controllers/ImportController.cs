﻿using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;

namespace CvBackend.Controllers
{
    [AllowAnonymous]
    [AllowCors]
    [RoutePrefix("api/import")]
    public class ImportController : ApiController
    {
        private const int ID_PROF = 1381; // 619, 945
        private const int ID_COMISIE = 2; // informatica

        [HttpPost]
        [Route("")]
        public async Task<IList> Import()
        {
            EvalUnivEntities context = new EvalUnivEntities();
            IQueryable<EvalUnivCriteriuProfesor> criteriaForProf = context
                .EvalUnivCriteriuProfesors
                .Where(c => c.ID_Profesor == ID_PROF);
            IQueryable<EvalUnivCriteriu> criteria = context
                .EvalUnivCriterius
                .Where(c => c.ID_EvalUnivComisieCNATDCU == ID_COMISIE);

            return await criteriaForProf
                .Join(criteria, c => c.ID_EvalUnivCriteriu, c => c.ID_EvalUnivCriteriu,
                    (critProf, crit) => new
                    {
                        id = critProf.ID_EvalUnivCriteriuProfesor,
                        values = critProf.ValoriConfigurareCampuriCriteriu,
                        type = crit.TipCriteriu
                    }
                ).ToListAsync();
        }

        [HttpGet]
        [Route("descriptions")]
        public async Task<IList> GetCriteriaDescriptions()
        {
            EvalUnivEntities context = new EvalUnivEntities();
            return await context.EvalUnivCriterius
                .Where(c => c.ID_EvalUnivComisieCNATDCU == ID_COMISIE)
                .Select(c => new
                {
                    type = c.TipCriteriu,
                    description = c.DenumireEvalUnivCriteriu
                })
                .ToListAsync();
        }

        [HttpPatch]
        [Route("sync")]
        public async Task<IHttpActionResult> Sync()
        {
            using (StreamReader sr = new StreamReader(HttpContext.Current.Request.InputStream))
            {
                string json = await sr.ReadToEndAsync();
                IList<EvalUnivCriteriuProfesor> criteria = JsonConvert.DeserializeObject<EvalUnivCriteriuProfesor[]>(json);
                if (criteria is null)
                {
                    return BadRequest();
                }

                EvalUnivEntities context = new EvalUnivEntities();
                using (DbContextTransaction tx = context.Database.BeginTransaction())
                {
                    foreach (EvalUnivCriteriuProfesor cr in criteria)
                    {
                        EvalUnivCriteriuProfesor criteriaToBeUpdated =
                            await context.EvalUnivCriteriuProfesors.SingleAsync(c => c.ID_EvalUnivCriteriuProfesor == cr.ID_EvalUnivCriteriuProfesor);
                        criteriaToBeUpdated.ValoriConfigurareCampuriCriteriu = cr.ValoriConfigurareCampuriCriteriu;
                    }

                    await context.SaveChangesAsync();
                    tx.Commit();
                    return Ok();
                }
            }
        }

        [HttpOptions]
        [Route("sync")]
        public IHttpActionResult SyncPreflight()
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "PATCH");
            return Ok();
        }
    }
}
