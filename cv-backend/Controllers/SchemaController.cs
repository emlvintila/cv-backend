﻿using System;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;

namespace CvBackend.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/schema")]
    [AllowCors]
    public class SchemaController : ApiController
    {
        private static readonly Lazy<object> Schema = new Lazy<object>(() =>
        {
            string json = File.ReadAllText(HttpContext.Current.Server.MapPath("~/zotero-schema.json"));

            return JsonConvert.DeserializeObject(json);
        }, LazyThreadSafetyMode.ExecutionAndPublication);

        [HttpGet]
        [Route("")]
        public IHttpActionResult GetSchema()
        {
            return Ok(Schema.Value);
        }

        [HttpGet]
        [Route("version")]
        public IHttpActionResult GetSchemaVersion()
        {
            return Ok((Schema.Value as dynamic).version);
        }
    }
}
