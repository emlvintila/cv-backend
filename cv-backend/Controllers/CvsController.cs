﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using CvBackend.Models;
using Newtonsoft.Json;

namespace CvBackend.Controllers
{
    [AllowAnonymous]
    [AllowCors]
    [RoutePrefix("api/cv")]
    public class CvsController : ApiController
    {
        [HttpGet]
        [Route("")]
        public IEnumerable<Cv> Index()
        {
            using (CvContext db = new CvContext())
            {
                return db.Cvs.ToList();
            }
        }

        [HttpGet]
        [Route("my")]
        public async Task<IHttpActionResult> GetMyCv()
        {
            using (CvContext db = new CvContext())
            {
                Cv cv = await db.Cvs.FirstOrDefaultAsync();
                if (!(cv is null))
                    return Ok(JsonConvert.DeserializeObject(cv.Json));
            }

            return NotFound();
        }

        [HttpPost]
        [Route("my")]
        public async Task<IHttpActionResult> UpdateMyCv()
        {
            string json = await Request.Content.ReadAsStringAsync();
            using (CvContext db = new CvContext())
            {
                Cv cv = await db.Cvs.FirstOrDefaultAsync() ?? db.Cvs.Add(new Cv());
                cv.Json = json;

                await db.SaveChangesAsync();
            }

            return Ok();
        }
    }
}
