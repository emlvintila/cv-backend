﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using CvBackend.Models;

namespace CvBackend.Controllers
{
    [AllowAnonymous]
    [AllowCors]
    [RoutePrefix("api/autocomplete")]
    public class AutocompleteController : ApiController
    {
        private static readonly Lazy<IList<Journal>> Journals = new Lazy<IList<Journal>>(() =>
        {
            return File.ReadAllLines(HttpContext.Current.Server.MapPath("~/journals.tsv"))
                .Select(row =>
                {
                    string[] parts = row.Split('\t');
                    return new Journal(parts[0], parts[1], parts[2]);
                })
                .ToList();
        }, LazyThreadSafetyMode.ExecutionAndPublication);

        private static readonly Lazy<IList<PublisherT>> Publishers = new Lazy<IList<PublisherT>>(() =>
        {
            return File.ReadAllLines(HttpContext.Current.Server.MapPath("~/publishers.tsv"))
                .Select(row =>
                {
                    string[] parts = row.Split('\t');
                    return new PublisherT(parts[0]);
                })
                .ToList();
        }, LazyThreadSafetyMode.ExecutionAndPublication);

        [HttpGet]
        [Route("journals")]
        public IEnumerable<Journal> GetJournalsAutocomplete()
        {
            NameValueCollection @params = HttpContext.Current.Request.Params;
            string issn = @params.Get("issn");
            if (!string.IsNullOrWhiteSpace(issn))
                return SearchIssn(issn);

            string title = @params.Get("title");
            if (!string.IsNullOrWhiteSpace(title))
                return SearchTitle(title);

            string abbr = @params.Get("abbr");
            // ReSharper disable once ConvertIfStatementToReturnStatement
            if (!string.IsNullOrWhiteSpace(abbr))
                return SearchAbbr(abbr);

            return Enumerable.Empty<Journal>();
        }

        [HttpGet]
        [Route("publishers")]
        public IEnumerable<PublisherT> GetPublishersAutocomplete()
        {
            string publisher = HttpContext.Current.Request.Params.Get("publisher");
            // ReSharper disable once ConvertIfStatementToReturnStatement
            if (!string.IsNullOrWhiteSpace(publisher))
                return SearchPublisher(publisher);

            return Enumerable.Empty<PublisherT>();
        }

        private IEnumerable<Journal> SearchIssn(string issn)
        {
            return Journals.Value.Where(journal => journal.Issn.ContainsCaseInsensitive(issn, GetCultureFromRequest()));
        }

        private IEnumerable<Journal> SearchTitle(string title)
        {
            return Journals.Value.Where(journal => journal.PublicationTitle.ContainsCaseInsensitive(title, GetCultureFromRequest()));
        }

        private IEnumerable<Journal> SearchAbbr(string abbr)
        {
            return Journals.Value.Where(journal => journal.JournalAbbreviation.ContainsCaseInsensitive(abbr, GetCultureFromRequest()));
        }

        private IEnumerable<PublisherT> SearchPublisher(string publisher)
        {
            return Publishers.Value.Where(p => p.Publisher.ContainsCaseInsensitive(publisher, GetCultureFromRequest()));
        }

        private static CultureInfo GetCultureFromRequest()
        {
            string language = (HttpContext.Current.Request.UserLanguages ?? Array.Empty<string>())
                // remove the ";q=0.9" part that browsers send to order languages by preference
                .Select(lang => lang.Split(';')[0])
                .FirstOrDefault();
            try
            {
                return language != null ? new CultureInfo(language) : CultureInfo.InvariantCulture;
            }
            catch (CultureNotFoundException)
            {
                return CultureInfo.InvariantCulture;
            }
        }
    }
}
